pdf:	main.tex
	@echo Compiling PDF file...
	@rubber --pdf main.tex 2>/dev/null
	@echo Done!

norub:	main.tex
	@echo Compiling PDF file...
	@pdflatex main.tex 2>/dev/null >/dev/null
	@echo done!

all:	main.tex

clean:
	@rm -f *.log
	@rm -f *.aux
	@rm -f *.pdf
	@rm -f *.toc
	@rm -f *.lot
	@rm -f *.lof
	@rm -f *.eps
